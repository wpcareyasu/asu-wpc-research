<a name="0.5.6"></a>
## 0.5.6 (2016-03-08)

* Updated gray-lighter-2 to be a bit darker. ([b2c05bf](https://bitbucket.org/dmarti18/asu-wpc-research/commits/b2c05bf))
* Updated version. ([d530b2f](https://bitbucket.org/dmarti18/asu-wpc-research/commits/d530b2f))
* Updating /dist files. ([3563f14](https://bitbucket.org/dmarti18/asu-wpc-research/commits/3563f14))



<a name="0.5.4"></a>
## 0.5.4 (2016-03-08)

* - Updated the base gray, because it was causing the lightest gray to be plain white. ([0bbe3b4](https://bitbucket.org/dmarti18/asu-wpc-research/commits/0bbe3b4))
* Matching version with live branch. ([282bafa](https://bitbucket.org/dmarti18/asu-wpc-research/commits/282bafa))
* Updating /dist files. ([29e8e53](https://bitbucket.org/dmarti18/asu-wpc-research/commits/29e8e53))



<a name="0.5.3"></a>
## 0.5.3 (2016-03-08)

* Modified the title class for headers to allow spacing on mobile. ([cea37eb](https://bitbucket.org/dmarti18/asu-wpc-research/commits/cea37eb))
* Updating /dist files. ([c5de470](https://bitbucket.org/dmarti18/asu-wpc-research/commits/c5de470))



<a name="0.5.1"></a>
## 0.5.1 (2016-03-07)

* Small CSS consistency updates. ([60522ba](https://bitbucket.org/dmarti18/asu-wpc-research/commits/60522ba))
* Updating /dist files. ([99c1767](https://bitbucket.org/dmarti18/asu-wpc-research/commits/99c1767))



<a name="0.5.0"></a>
# 0.5.0 (2016-03-07)

* Added custom login screen, W. P. Carey themed. ([d6e6ccd](https://bitbucket.org/dmarti18/asu-wpc-research/commits/d6e6ccd))
* Adding Google Font Roboto to theme. ([32c3a62](https://bitbucket.org/dmarti18/asu-wpc-research/commits/32c3a62))
* Fixed the hr color ([21b3fd6](https://bitbucket.org/dmarti18/asu-wpc-research/commits/21b3fd6))
* Updating /dist files. ([924c2cb](https://bitbucket.org/dmarti18/asu-wpc-research/commits/924c2cb))



<a name="0.4.0"></a>
# 0.4.0 (2016-03-07)

* - Reworked the customizer options and the way the header color is treated. ([c38deff](https://bitbucket.org/dmarti18/asu-wpc-research/commits/c38deff))
* Updating /dist files. ([aebde3c](https://bitbucket.org/dmarti18/asu-wpc-research/commits/aebde3c))



<a name="0.3.9"></a>
## 0.3.9 (2016-03-04)

* Adding js to fire off table sorter if table is present. ([946f909](https://bitbucket.org/dmarti18/asu-wpc-research/commits/946f909))
* Updated version. ([2f2ac7a](https://bitbucket.org/dmarti18/asu-wpc-research/commits/2f2ac7a))
* Updating /dist files. ([14c878f](https://bitbucket.org/dmarti18/asu-wpc-research/commits/14c878f))



<a name="0.3.7"></a>
## 0.3.7 (2016-03-04)

* Added styling for full width/fluid template. And some well added bootstrap .well styling. ([881b27c](https://bitbucket.org/dmarti18/asu-wpc-research/commits/881b27c))
* Fixed an issue with the incorrect closing of a div tag. ([efcc9be](https://bitbucket.org/dmarti18/asu-wpc-research/commits/efcc9be))
* Updating /dist files. ([3c2c153](https://bitbucket.org/dmarti18/asu-wpc-research/commits/3c2c153))



<a name="0.3.4"></a>
## 0.3.4 (2016-03-03)

* Previous commit didn't have the new stylesheet file. ([77bc91b](https://bitbucket.org/dmarti18/asu-wpc-research/commits/77bc91b))



<a name="0.3.3"></a>
## 0.3.3 (2016-03-03)

* - Changed the excerpts read more, so that it appears as block element instead of inline in the excer ([8fb3f59](https://bitbucket.org/dmarti18/asu-wpc-research/commits/8fb3f59))
* Updating /dist files. ([17c1bdc](https://bitbucket.org/dmarti18/asu-wpc-research/commits/17c1bdc))



<a name="0.3.1"></a>
## 0.3.1 (2016-03-02)

* Fix an issue where buttons weren't getting the proper spacing between them. Had to restructure some  ([adfa831](https://bitbucket.org/dmarti18/asu-wpc-research/commits/adfa831))
* Updating /dist files. ([898a4a8](https://bitbucket.org/dmarti18/asu-wpc-research/commits/898a4a8))



<a name="0.3.0"></a>
# 0.3.0 (2016-03-02)

* Big Style Update ([52e0303](https://bitbucket.org/dmarti18/asu-wpc-research/commits/52e0303))
* Updated version number. ([322649d](https://bitbucket.org/dmarti18/asu-wpc-research/commits/322649d))
* Updating /dist files. ([599b385](https://bitbucket.org/dmarti18/asu-wpc-research/commits/599b385))



<a name="0.2.1"></a>
## 0.2.1 (2016-02-25)

* Updating /dist files. ([a26d222](https://bitbucket.org/dmarti18/asu-wpc-research/commits/a26d222))



<a name="0.2.0"></a>
# 0.2.0 (2016-02-25)

* Added a max-width to images inside post to prevent from going over their container on mobile. ([733e313](https://bitbucket.org/dmarti18/asu-wpc-research/commits/733e313))
* Updated version. ([e1758e5](https://bitbucket.org/dmarti18/asu-wpc-research/commits/e1758e5))



<a name="0.1.18"></a>
## 0.1.18 (2016-02-24)

* Updating /dist files. ([e5aea43](https://bitbucket.org/dmarti18/asu-wpc-research/commits/e5aea43))



<a name="0.1.17"></a>
## 0.1.17 (2016-02-24)

* Changed header-primary widget background to white, Fix font-weight for the slideshow on mobile. ([e951d8c](https://bitbucket.org/dmarti18/asu-wpc-research/commits/e951d8c))
* Updated version to match live branch. ([39c689f](https://bitbucket.org/dmarti18/asu-wpc-research/commits/39c689f))
* Updating version. ([46da385](https://bitbucket.org/dmarti18/asu-wpc-research/commits/46da385))



<a name="0.1.15"></a>
## 0.1.15 (2016-02-24)

* Added and if statement to mobile navigation, so that it only show the mobile toggle if the site has  ([087c15d](https://bitbucket.org/dmarti18/asu-wpc-research/commits/087c15d))
* Added the sidebar header-primary widget to every page. ([ab17939](https://bitbucket.org/dmarti18/asu-wpc-research/commits/ab17939))
* Added the sidebar header-primary widget to every page. ([39f2c6e](https://bitbucket.org/dmarti18/asu-wpc-research/commits/39f2c6e))
* Fixed the label so it doesn't show up since it's meant just for screen reading text. ([1704725](https://bitbucket.org/dmarti18/asu-wpc-research/commits/1704725))
* Ported over existing templates from the Centers theme. ([0b6b5e0](https://bitbucket.org/dmarti18/asu-wpc-research/commits/0b6b5e0))
* Reworked the way the main styling for content regions so that it's more consistent. ([91e3184](https://bitbucket.org/dmarti18/asu-wpc-research/commits/91e3184))
* Reworking the way the header-primary is being called, previous method didn't work. ([3787372](https://bitbucket.org/dmarti18/asu-wpc-research/commits/3787372))
* Updated sidebar display function to use is_home() instead of frontage to make sure static front page ([901035a](https://bitbucket.org/dmarti18/asu-wpc-research/commits/901035a))
* Updated the search form structure, so that it properly displays on mobile. ([9baaf58](https://bitbucket.org/dmarti18/asu-wpc-research/commits/9baaf58))
* updating the /dist files ([f5c379c](https://bitbucket.org/dmarti18/asu-wpc-research/commits/f5c379c))
* Updating version number ([69d1a2d](https://bitbucket.org/dmarti18/asu-wpc-research/commits/69d1a2d))
* Updating version. ([78ce53c](https://bitbucket.org/dmarti18/asu-wpc-research/commits/78ce53c))



<a name="0.1.7"></a>
## 0.1.7 (2016-02-24)

* Added pages to the main content region styling. Also updated the version in this branch to match to  ([97ce558](https://bitbucket.org/dmarti18/asu-wpc-research/commits/97ce558))
* Updated Updated /dist files for new changes for v0.1.7 ([e36f899](https://bitbucket.org/dmarti18/asu-wpc-research/commits/e36f899))



<a name="0.1.6"></a>
## 0.1.6 (2016-02-24)

* Adding styles for the wordpress categories widget. ([a6a998b](https://bitbucket.org/dmarti18/asu-wpc-research/commits/a6a998b))
* Updated Updated /dist files for new changes for v0.1.6 ([a77a283](https://bitbucket.org/dmarti18/asu-wpc-research/commits/a77a283))



<a name="0.1.5"></a>
## 0.1.5 (2016-02-24)

* Added needed margin-bottom for buttons. Hid the date from post. ([93e148d](https://bitbucket.org/dmarti18/asu-wpc-research/commits/93e148d))
* This file was not intended to be pushed to the main branch, it's a live branch production file. ([cba72a8](https://bitbucket.org/dmarti18/asu-wpc-research/commits/cba72a8))
* Updated /dist files for new changes. ([1893560](https://bitbucket.org/dmarti18/asu-wpc-research/commits/1893560))
* Updated version. ([92551a9](https://bitbucket.org/dmarti18/asu-wpc-research/commits/92551a9))



<a name="0.1.4"></a>
## 0.1.4 (2016-02-24)

* Forgot to add the new stylesheet for the previous versions. ([a036d1a](https://bitbucket.org/dmarti18/asu-wpc-research/commits/a036d1a))
* Forgot to update the version in the style.css file. Doing it now for v0.1.2 ([07880de](https://bitbucket.org/dmarti18/asu-wpc-research/commits/07880de))
* trying tagging through a different method, to see if GitHub updater sees the new version. ([ed8fa7b](https://bitbucket.org/dmarti18/asu-wpc-research/commits/ed8fa7b))
* Updating to increase the number to toggle a push, since latest live branch commit didn't include the ([fc63ff0](https://bitbucket.org/dmarti18/asu-wpc-research/commits/fc63ff0))
* Updating version. ([a895f60](https://bitbucket.org/dmarti18/asu-wpc-research/commits/a895f60))



<a name="0.1.1"></a>
## 0.1.1 (2016-02-24)

* Adding dist file for v0.1.0 ([39d3526](https://bitbucket.org/dmarti18/asu-wpc-research/commits/39d3526))
* Updated dist css for v0.1.1 ([36498be](https://bitbucket.org/dmarti18/asu-wpc-research/commits/36498be))
* Updated the buttons widgets to include a blue programs option. Also fixed a css issue with an incorr ([48d9fb9](https://bitbucket.org/dmarti18/asu-wpc-research/commits/48d9fb9))



<a name="0.1.0"></a>
# 0.1.0 (2016-02-23)

* Added a white background to the tinymce css so that it doesn't take the gray site background. ([9eec644](https://bitbucket.org/dmarti18/asu-wpc-research/commits/9eec644))
* Added custom bootstrap buttons that align with ASU's web standards. ([3d3a0b8](https://bitbucket.org/dmarti18/asu-wpc-research/commits/3d3a0b8))
* Added custom bootstrap variables along with global css for body and content regions. ([25b361e](https://bitbucket.org/dmarti18/asu-wpc-research/commits/25b361e))
* Added Font-Awesome, Bootstrap-Select, Lightbox2, and TableSorter to the bower file. ([5d8eb19](https://bitbucket.org/dmarti18/asu-wpc-research/commits/5d8eb19))
* Added tgmpa to recommend plugins to install. ([9927c4c](https://bitbucket.org/dmarti18/asu-wpc-research/commits/9927c4c))
* Added the header widget area. ([8f916bf](https://bitbucket.org/dmarti18/asu-wpc-research/commits/8f916bf))
* Adding bootstraps navwalker to the theme. ([489e47a](https://bitbucket.org/dmarti18/asu-wpc-research/commits/489e47a))
* Adding stylesheets for the slideshow and login page. ([e7ac4c3](https://bitbucket.org/dmarti18/asu-wpc-research/commits/e7ac4c3))
* Big update: Homepage, header, footer and theme customization options. ([6f7cdf8](https://bitbucket.org/dmarti18/asu-wpc-research/commits/6f7cdf8))
* Change the version to reflect the tag for the upcoming release. ([40c8f6c](https://bitbucket.org/dmarti18/asu-wpc-research/commits/40c8f6c))
* Cleaning up the search form file. ([e076754](https://bitbucket.org/dmarti18/asu-wpc-research/commits/e076754))
* Cleanup the file by adding consistent spacing. Also added the call to the tgmpa plugin so that it ac ([4700919](https://bitbucket.org/dmarti18/asu-wpc-research/commits/4700919))
* Fixed an issue where the reference to the font awesome component wasn't being included because a mis ([96e1c1c](https://bitbucket.org/dmarti18/asu-wpc-research/commits/96e1c1c))
* Importing gitignore file from sage 8.4.2 ([4517360](https://bitbucket.org/dmarti18/asu-wpc-research/commits/4517360))
* Initial commit of unmodified Sage 8.4.2. ([2bc70d1](https://bitbucket.org/dmarti18/asu-wpc-research/commits/2bc70d1))
* Modified CSS style for post-navigation to add margin-bottom. ([ac5a310](https://bitbucket.org/dmarti18/asu-wpc-research/commits/ac5a310))
* Modified the customize options for the theme so that it it properly styles the header and only adds  ([671893a](https://bitbucket.org/dmarti18/asu-wpc-research/commits/671893a))
* Page and sidebar styles ([75d0e1b](https://bitbucket.org/dmarti18/asu-wpc-research/commits/75d0e1b))
* Previous commit was incomplete. This commit adds the navwalker php into the functions file. Also add ([fd6c4a1](https://bitbucket.org/dmarti18/asu-wpc-research/commits/fd6c4a1))
* Removed the continued link from the excerpt. ([5b91291](https://bitbucket.org/dmarti18/asu-wpc-research/commits/5b91291))
* Replace the standard Wordpress search form for a custom made version. ([3aaa815](https://bitbucket.org/dmarti18/asu-wpc-research/commits/3aaa815))
* Small spacing update. ([c555792](https://bitbucket.org/dmarti18/asu-wpc-research/commits/c555792))
* Updated form to include new wp_core search form structure. ([d6c93bf](https://bitbucket.org/dmarti18/asu-wpc-research/commits/d6c93bf))
* Updated information on the style.css file to reflect then correct theme information including the bi ([d91b5aa](https://bitbucket.org/dmarti18/asu-wpc-research/commits/d91b5aa))
* Updated navbar-toggle and some some other mobile menu header css so that it looks good even when the ([31d5932](https://bitbucket.org/dmarti18/asu-wpc-research/commits/31d5932))
* Updated search template to use the same loop template as the homepage. ([6a8933e](https://bitbucket.org/dmarti18/asu-wpc-research/commits/6a8933e))
* Updated the box-shadow to use the mixin used a across others styles. Also simplified the css for the ([0a0d554](https://bitbucket.org/dmarti18/asu-wpc-research/commits/0a0d554))
* Updated the index.php to behave differently on the homepage vs a category results page. Created a se ([3cd347c](https://bitbucket.org/dmarti18/asu-wpc-research/commits/3cd347c))



