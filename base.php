<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } ?>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <div class="header-widget <?php echo get_theme_mod('asu_research_header_widget_preset', 'container'); ?> <?php echo get_theme_mod('asu_research_header_widget_size_preset', ''); ?>" role="complementary">
          <?php dynamic_sidebar('header-primary'); ?>
    </div>
    <div class="container">
      <?php if (is_home()) :  ?>
          <?php $paged = ( get_query_var('paged') ? get_query_var('paged') : 1); ?>
          <?php $featured_query = new WP_Query( array( 'tag' => 'featured', 'posts_per_page' => '1' ) );?>
          <?php if ($featured_query->have_posts() && $paged < 2) : ?>
            <?php while ($featured_query->have_posts()) : $featured_query->the_post(); ?>
              <?php get_template_part('templates/content-featured', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
            <?php endwhile; ?>
          <?php endif; ?>
          <?php wp_reset_postdata(); ?>
      <?php endif; ?>
    </div>
    <div class="wrap container" role="document">
      <div class="content row">
        <main class="main">
          <div class="wrapper">
            <?php include Wrapper\template_path(); ?>
          </div>
        </main><!-- /.main -->
        <?php if (Setup\display_sidebar()) : ?>
          <aside class="sidebar">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
