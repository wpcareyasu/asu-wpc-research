(function($) {
  if ($('a[data-target="#plusModal"]').length > 0) {
    $('body').append('<div class="modal fade" id="plusModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">' +
      '<div class="modal-dialog" role="document">' +
      '    <div class="modal-content">' +
      '      <div class="modal-header">' +
      '        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
      '        <h4 class="modal-title" id="myModalLabel">Modal title</h4>' +
      '      </div>' +
      '      <div class="modal-body">' +
      '        ...' +
      '      </div>' +
      '      <div class="modal-footer">' +
      '        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
      '      </div>' +
      '    </div>' +
      '  </div>' +
      '</div>');

    var convertAttributes = function(attr, type) {
      var rawAttributes = attr.split('&'),
        attrObj = [],
        key = [],
        i = "";

      for (i = 0; i < rawAttributes.length; i++) {
        key = rawAttributes[i].split('=');
        attrObj[key[0]] = key[1];
      }

      //Check for empty propoerties and apply defaults if empty
      if (type === 'image') {
        if (typeof attrObj.class === 'undefined') {
          attrObj.class = 'img-responsive';

        }
      } else if (type === 'iframe') {
        if (typeof attrObj.width === 'undefined') {
          attrObj.width = '100%';
        }

        if (typeof attrObj.height === 'undefined') {
          attrObj.height = '350px';
        }
      }

      //Convert results into html attributes
      var combination = '';
      for (var prop in attrObj) {
        if (attrObj.hasOwnProperty(prop)) {
          combination += prop + '="' + attrObj[prop] + '" ';
        }
      }
      return combination;
    };

    $('#plusModal').on('show.bs.modal', function(event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var source = button.data('url'); // Extract info from data-* attributes
      var type = button.data('type');
      var title = button.data('title');
      var size = button.data('size');
      var attr = button.data('attr');
      attr = attr ? convertAttributes(attr, type) : '';
      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this);
      modal.find('.modal-title').text(title);
      modal.find('.modal-dialog').addClass(size);
      if (type === 'image') {
        modal.find('.modal-body').html('<img ' + ( attr? attr : 'class="img-responsive"' ) + ' src="' + source + '">');
      } else if (type === 'iframe') {
        modal.find('.modal-body').html('<iframe ' + ( attr? attr : 'width="100%" height="350"' ) + ' src="' + source + '" frameborder="0"></iframe>');
      }

    });
    $('#plusModal').on('hidden.bs.modal', function(e) {
      jQuery('.modal-body', this).html('');
    });
  }

})(jQuery); // Fully reference jQuery after this point.
