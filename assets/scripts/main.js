/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {
  function menuThing(element) {
    var mobileCheck = $('.navbar-nav>li').css('float') === "none";
    var $parent = $(element).parent();
    var activePath = 'li.menu-item-has-children[class*="current-menu"]';

    if ($parent.is(activePath) && mobileCheck) {
      //Close any other opened navbar item within the same navbar
      if ($parent.hasClass('closed')) {
        $parent.removeClass('closed');
        $parent.parent().find('.open').removeClass('open');
      } else {
        $parent.addClass('closed');
      }
    } else if ($parent.hasClass('open')) {
      $parent.removeClass('open');
    } else {
      $parent.parent().find('.open').removeClass('open');
      $parent.addClass('open');
      if (mobileCheck) {
        $parent.parent().find(activePath).addClass('closed');
      }
    }
  }

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
        $('article.feed').click(function() {
          window.open($('.entry-title a', this).attr('href'), '_self');
        });

        /*
         *  Navbar Menu
         */

        $('.navbar-nav>li.dropdown>a').click(function() {
          menuThing(this);
          return false;
        });
        $('.navbar-nav .dropdown-submenu>a').click(function() {
          menuThing(this);
          return false;
        });

        /*
         *  Navbar Modal
         */
        $('.menu-btn.modal-link a').click(function() {
          $('#menu-modal').modal('show');
        });

        /*
         *  Table Sortable
         */
        if ($('table.sortable').length > 0) {
          $('.sortable').tablesorter();
        }

        /*
         *  Expanding Menu Functionality
         */
        if ($('.sidebar .widget_nav_menu').length > 0 || $('.sidebar .widget_advanced_menu').length > 0) {

          $('.sidebar .menu li a').click(function(e) {
            var $parent = $(this).parent();
            if ($(this).attr('href') === '#') {
              if ($parent.hasClass("open")) {
                $parent.removeClass('open').addClass('closed');
                return false;
              } else if ($parent.hasClass("closed")) {
                $parent.addClass('open').removeClass('closed');
                return false;
              } else if ($parent.hasClass('current-menu-parent') || $parent.hasClass('current-menu-ancestor')) {
                $parent.addClass('closed');
                return false;
              } else {
                $parent.addClass('open');
                return false;
              }
            }
          });

        }
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS

        //Mobile Flexslider  - Make each slide the same size based on the biggest slide for styling consistency.
        var fixedImage = function() {
          if ($('.flexslider .slides').height() > parseInt($('.flexslider .slides li').css('min-height'))) {
            var containerHeght = $('.flexslider .slides').height();
            $('.flexslider .slides li > img').css('height', containerHeght);
          } else {
            $('.flexslider .slides li > img').css('height', '');
          }
        };

        $(window).load(function() {
          if ($('.flexslider .slides').length > 0) {
            fixedImage();
            $(window).on('resize', function() {
              fixedImage();
            });
          }
        });
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
