(function($) {
  // Site title
  wp.customize('blogname', function(value) {
    value.bind(function(to) {
      $('.mobile-header a').text(to);
    });
  });

  wp.customize('asu_research_header_preset', function(value) {
    value.bind(function(newval) {
      $('.navbar').removeClass(function (index, css) {
        return (css.match (/(^|\s)navbar-header-\S+/g) || []).join(' ');
      }).addClass(newval);
    });
  });
  
  wp.customize('asu_research_menu_preset', function(value) {
    value.bind(function(newval) {
      $('.navbar').removeClass(function (index, css) {
        return (css.match (/(^|\s)navbar-menu-\S+/g) || []).join(' ');
      }).addClass(newval);
    });
  });

  wp.customize('asu_research_footer_preset', function(value) {
    value.bind(function(newval) {
      $('footer.content-info').removeClass(function (index, css) {
        return (css.match (/(^|\s)footer-\S+/g) || []).join(' ');
      }).addClass(newval);
      var $brandedLogo = $('footer.content-info .branded-logo > img');
      var logoSrc = $brandedLogo.attr('src');
      var srcFlipper;
      if(newval === 'footer-light'){
        srcFlipper = logoSrc.replace('-reversed', '');
        $brandedLogo.attr('src', srcFlipper);
      }else{
        srcFlipper = logoSrc.replace('.png', '-reversed.png');
        $brandedLogo.attr('src', srcFlipper);
      }
    });
  });

  wp.customize('asu_research_footer_widget_preset', function(value) {
    value.bind(function(newval) {
      $('footer.content-info').removeClass(function (index, css) {
        return (css.match (/(^|\s)widget-\S+/g) || []).join(' ');
      }).addClass(newval);
    });
  });

})(jQuery);
