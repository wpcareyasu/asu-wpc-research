<?php

namespace Roots\Sage\Assets;

/**
 * Get paths for assets
 */
class JsonManifest {
  private $manifest;

  public function __construct($manifest_path) {
    if (file_exists($manifest_path)) {
      $this->manifest = json_decode(file_get_contents($manifest_path), true);
    } else {
      $this->manifest = [];
    }
  }

  public function get() {
    return $this->manifest;
  }

  public function getPath($key = '', $default = null) {
    $collection = $this->manifest;
    if (is_null($key)) {
      return $collection;
    }
    if (isset($collection[$key])) {
      return $collection[$key];
    }
    foreach (explode('.', $key) as $segment) {
      if (!isset($collection[$segment])) {
        return $default;
      } else {
        $collection = $collection[$segment];
      }
    }
    return $collection;
  }
}

function asset_path($filename) {
  $dist_path = get_template_directory_uri() . '/dist/';
  $directory = dirname($filename) . '/';
  $file = basename($filename);
  static $manifest;

  if (empty($manifest)) {
    $manifest_path = get_template_directory() . '/dist/' . 'assets.json';
    $manifest = new JsonManifest($manifest_path);
  }

  if (array_key_exists($file, $manifest->get())) {
    return $dist_path . $directory . $manifest->get()[$file];
  } else {
    return $dist_path . $directory . $file;
  }
}

/**
 * Customize login screen
 */
function custom_login_css() {
  echo '<link rel="stylesheet" type="text/css" href="'. asset_path('styles/login.css') . '" >';
}
add_action('login_head', __NAMESPACE__ . '\\custom_login_css');

function custom_login_logo_url() {
  return get_bloginfo( 'url' );
}

add_filter( 'login_headerurl', __NAMESPACE__ . '\\custom_login_logo_url' );

function custom_login_logo_url_title() {
  return get_bloginfo( 'name' );
}
add_filter( 'login_headertitle', __NAMESPACE__ . '\\custom_login_logo_url_title' );

function custom_login_message() {
  return '<h2>' . get_bloginfo( 'name' ) . '</h2>';
}
add_filter('login_message', __NAMESPACE__ . '\\custom_login_message');