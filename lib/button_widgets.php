<?php

/*
Plugin Name: Button Widgets
Plugin URI: http://wpcarey.asu.edu
Description: Create button widgets
Author: Danny Martinez
Version: 1
Author URI: http://martinezd.com
*/

class wpc_bootstrap_buttons extends WP_Widget
{
    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct('wpc_bootstrap_buttons',
         // Base ID
        __('Buttons', 'text_domain'),
         // Name
        array('description' => __('Add bootstrap themed buttons.', 'text_domain'),)
         // Args
        );
    }
    
    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance) {
        // these are the widget options
        $title = apply_filters('widget_title', $instance['title']);
        $link = $instance['link'];
        $size = $instance['size'];
        $type = $instance['type'];
        $target = $instance['target'];

        echo $args['before_widget'];
        if ($link) {
            echo '<a class="btn btn-block ' . $type . ' ' . $size .'" href="' . $link . '" target="' . $target . '">' . $title . '</a>';
        }
        echo $args['after_widget'];
    }
    
    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form($instance) {
        $title = !empty($instance['title']) ? $instance['title'] : __('', 'text_domain');
        $link = !empty($instance['link']) ? $instance['link'] : __('http://', 'text_domain');
        $type = !empty($instance['type']) ? $instance['type'] : __('btn-default', 'text_domain');
        $size = !empty($instance['size']) ? $instance['size'] : __('btn-deafult', 'text_domain');
        $target = !empty($instance['target']) ? $instance['target'] : __('_self', 'text_domain');
?>
<p>
    <label for="<?php echo $this->get_field_id('title'); ?>">
        <?php _e( 'Title', 'bootstrap_buttons'); ?>
    </label>
    <input class="widefat" id="<?php
        echo $this->get_field_id('title'); ?>" name="<?php
        echo $this->get_field_name('title'); ?>" type="text" value="<?php
        echo $title; ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id('link'); ?>">
        <?php _e( 'Link:', 'bootstrap_buttons');?>
    </label>
    <input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" rows="7" cols="20" value="<?php echo $link; ?>">
</p>
<p>
<p>
    <label for="<?php echo $this->get_field_id('type'); ?>">
        <?php _e( 'Type:', 'bootstrap_buttons'); ?>
    </label>
    <select name="<?php echo $this->get_field_name('type'); ?>" id="<?php echo $this->get_field_id('type'); ?>" class="widefat">
        <?php $type_options=array( 'Default' => 'btn-default', 'Primary' => 'btn-primary', 'Secondary' => 'btn-secondary', 'Call to Action' => 'btn-cta', 'Plain Link' => 'btn-link', 'Programs(blue)' => 'btn-programs' ); foreach ($type_options as $key => $option) { echo '
        <option value="' . $option . '" id="' . $option . '" ', $type == $option ? ' selected="selected" ' : ' ', '>', $key, '</option>'; } ?>
    </select>
</p>
<p>
    <label for="<?php echo $this->get_field_id('size'); ?>">
        <?php _e( 'Size:', 'bootstrap_buttons'); ?>
    </label>
    <select name="<?php echo $this->get_field_name('size'); ?>" id="<?php echo $this->get_field_id('size'); ?>" class="widefat">
        <?php $size_options=array( 'Default'=> '_self', 'Large' => 'btn-lg', 'Small' => 'btn-sm', 'X-Small' => 'btn-xs' ); foreach ($size_options as $key => $value) { echo '
        <option value="' . $value . '" id="' . $value . '" ', $size == $value ? ' selected="selected" ' : ' ', '>', $key, '</option>'; } ?>
    </select>
</p>
<p>
    <label for="<?php echo $this->get_field_id('target'); ?>">
        <?php _e( 'Target:', 'bootstrap_buttons'); ?>
    </label>
    <select name="<?php echo $this->get_field_name('target'); ?>" id="<?php echo $this->get_field_id('target'); ?>" class="widefat">
        <?php $target_options=array(
            'Default'=> '',
            'New Window or Tab' => '_blank'
            );
        foreach ($target_options as $key => $value) {
            echo '<option value="' . $value . '" id="' . $value . '" ', $target == $value ? ' selected="selected" ' : ' ', '>', $key, '</option>';
        } ?>
    </select>
</p>
<?php
    }
    
    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['link'] = (!empty($new_instance['link'])) ? strip_tags($new_instance['link']) : '';
        $instance['type'] = (!empty($new_instance['type'])) ? strip_tags($new_instance['type']) : '';
        $instance['size'] = (!empty($new_instance['size'])) ? strip_tags($new_instance['size']) : '';
        $instance['target'] = (!empty($new_instance['target'])) ? strip_tags($new_instance['target']) : '';
        
        return $instance;
    }
}

 // class wpc_bootstrap_buttons
add_action( 'widgets_init', function(){
     register_widget( 'wpc_bootstrap_buttons' );
});
