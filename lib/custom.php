<?php

function getLatestFeatureID(){
    $featured_query = new WP_Query( array( 'tag' => 'featured', 'posts_per_page' => '1' ) );
    while ($featured_query->have_posts()) : $featured_query->the_post();
        return $featured_query->post->ID;
    endwhile;
}

function modify_main_query( $query ) {
    //exclude featured post in main_query 
    if ( $query->is_home() && $query->is_main_query() ) { // Run only on the homepage
        $exclude_post_id = getLatestFeatureID();
        $query->query_vars['post__not_in'] = array($exclude_post_id); // Exclude my featured category because I display that elsewhere
        $query->query_vars['posts_per_page'] = 5; // Show only 5 posts on the homepage only
    }
}
// Hook my above function to the pre_get_posts action
add_action( 'pre_get_posts', 'modify_main_query' );

add_image_size( 'featured-retina', 1380, 1104, array( 'left', 'top' ) );
add_image_size( 'featured', 690, 552, array( 'left', 'top' ) );
add_image_size( 'feed', 434, 346, array( 'left', 'top' ) );

//Allow &nbsp; in tinymce
function allow_nbsp_in_tinymce( $mceInit ) {
    $mceInit['entities'] = '160,nbsp,38,amp,60,lt,62,gt';   
    $mceInit['entity_encoding'] = 'named';
    return $mceInit;
}

add_filter('tiny_mce_before_init', __NAMESPACE__ . '\\allow_nbsp_in_tinymce');