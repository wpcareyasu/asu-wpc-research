<?php

namespace Roots\Sage\Customizer;

use Roots\Sage\Assets;

/**
 * Add postMessage support
 */
function customize_register($wp_customize) {

	/*Header Preset Section*/
	$wp_customize->add_section( 'asu_research_header_presets_options',
	    array(
	        'title'      => __('Header Presets','asu_research'),
	        'priority'   => 30,
	    )
	);
	
	//Header Preset - Header Color
	$wp_customize->add_setting('asu_research_header_preset',
	    array(
	        'default'   => '', //Default setting/value to save
	        'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
	    )
	);
	$wp_customize->add_control(
	    'asu_research_header_presets', 
	    array(
	        'label'    => __( 'Header Style Presets', 'asu_research' ),
	        'section'  => 'asu_research_header_presets_options',
	        'settings' => 'asu_research_header_preset',
	        'type'     => 'radio',
	        'choices'  => array(
				''                     => 'Default',
				'navbar-header-gold'   => 'Gold',
				'navbar-header-maroon' => 'Maroon'
	        ),
	    )
	);

	//Header Preset - Menu Color
	$wp_customize->add_setting('asu_research_menu_preset',
	    array(
	        'default'   => '', //Default setting/value to save
	        'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
	    )
	);
	$wp_customize->add_control(
	    'asu_research_menu_presets', 
	    array(
	        'label'    => __( 'Menu Style Presets', 'asu_research' ),
	        'section'  => 'asu_research_header_presets_options',
	        'settings' => 'asu_research_menu_preset',
	        'type'     => 'radio',
	        'choices'  => array(
				''                    => 'Default',
				'navbar-menu-dark' 	  => 'Dark'
	        ),
	    )
	);

	//Header Preset - Header Name
	$wp_customize->add_setting('asu_research_header_name_preset',
	    array(
	        'default'   => '', //Default setting/value to save
	        'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
	    )
	);
	$wp_customize->add_control(
	    'asu_research_header_name_presets', 
	    array(
	        'label'    => __( 'Site Title', 'asu_research' ),
	        'section'  => 'asu_research_header_presets_options',
	        'settings' => 'asu_research_header_name_preset',
	        'type'     => 'radio',
	        'choices'  => array(
				''                 => 'Show',
				'visible-xs-block' => 'Hide'
	        ),
	    )
	);

	//Header Preset - Menu Layout
	$wp_customize->add_setting('asu_research_menu_layout_preset',
	    array(
	        'default'   => '', //Default setting/value to save
	        'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
	    )
	);
	$wp_customize->add_control(
	    'asu_research_menu_layout_presets', 
	    array(
	        'label'    => __( 'Menu Layout Presets', 'asu_research' ),
	        'section'  => 'asu_research_header_presets_options',
	        'settings' => 'asu_research_menu_layout_preset',
	        'type'     => 'radio',
	        'choices'  => array(
				''             => 'Default',
				'navbar-alt'   => 'Alternative'
	        ),
	    )
	);

	//Header Preset - Header Widget
	$wp_customize->add_setting('asu_research_header_widget_preset',
	    array(
	        'default'   => 'container', //Default setting/value to save
	        'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
	    )
	);
	$wp_customize->add_control(
	    'asu_research_header_widget_presets', 
	    array(
	        'label'    => __( 'Header Widget Layout', 'asu_research' ),
	        'section'  => 'asu_research_header_presets_options',
	        'settings' => 'asu_research_header_widget_preset',
	        'type'     => 'radio',
	        'choices'  => array(
				'container'       => 'Default',
				'container-fluid' => 'Full'
	        ),
	    )
	);

	//Header Preset - Header Widget Size
	$wp_customize->add_setting('asu_research_header_widget_size_preset',
	    array(
	        ''   => 'container', //Default setting/value to save
	        'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
	    )
	);
	$wp_customize->add_control(
	    'asu_research_header_widget_size_presets', 
	    array(
	        'label'    => __( 'Header Widget Layout (Homepage & Full Layout Only)', 'asu_research' ),
	        'section'  => 'asu_research_header_presets_options',
	        'settings' => 'asu_research_header_widget_size_preset',
	        'type'     => 'radio',
	        'choices'  => array(
				'' => 'Default (380px)',
				'header-widget-large' => 'Large (490px)'
	        ),
	    )
	);

	//Post Preset Section
	$wp_customize->add_section( 'asu_research_post_presets_options',
	    array(
	        'title'      => __('Post Presets','asu_research'),
	        'priority'   => 30,
	    )
	);

	//Post Preset - Post Author
	$wp_customize->add_setting('asu_research_post_author',
	    array(
	        'default'   => true, //Default setting/value to save
	        'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
	    )
	);
	$wp_customize->add_control(
	    'asu_research_post_author', 
	    array(
	        'label'    => __( 'Author', 'asu_research' ),
	        'section'  => 'asu_research_post_presets_options',
	        'settings' => 'asu_research_post_author',
	        'type'     => 'radio',
	        'choices'  => array(
				true      => 'Show',
				false 	  => 'Hide'
	        ),
	    )
	);

	//Post Preset - Post Tags
	$wp_customize->add_setting('asu_research_post_tags',
	    array(
	        'default'   => true, //Default setting/value to save
	        'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
	    )
	);
	$wp_customize->add_control(
	    'asu_research_post_tags', 
	    array(
	        'label'    => __( 'Tags', 'asu_research' ),
	        'section'  => 'asu_research_post_presets_options',
	        'settings' => 'asu_research_post_tags',
	        'type'     => 'radio',
	        'choices'  => array(
				true      => 'Show',
				false 	  => 'Hide'
	        ),
	    )
	);
	//Post Preset - Post Excerpt
	$wp_customize->add_setting('asu_research_post_excerpt',
	    array(
	        'default'   => true, //Default setting/value to save
	        'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
	    )
	);
	$wp_customize->add_control(
	    'asu_research_post_excerpt', 
	    array(
	        'label'    => __( 'Excerpt', 'asu_research' ),
	        'section'  => 'asu_research_post_presets_options',
	        'settings' => 'asu_research_post_excerpt',
	        'type'     => 'radio',
	        'choices'  => array(
				true      => 'Show',
				false 	  => 'Hide'
	        ),
	    )
	);
	//Post Preset - Post Tags
	$wp_customize->add_setting('asu_research_post_tags',
	    array(
	        'default'   => true, //Default setting/value to save
	        'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
	    )
	);
	$wp_customize->add_control(
	    'asu_research_post_tags', 
	    array(
	        'label'    => __( 'Tags', 'asu_research' ),
	        'section'  => 'asu_research_post_presets_options',
	        'settings' => 'asu_research_post_tags',
	        'type'     => 'radio',
	        'choices'  => array(
				true      => 'Show',
				false 	  => 'Hide'
	        ),
	    )
	);

	//Post Preset - Post Timestamp
	$wp_customize->add_setting('asu_research_post_timestamp',
	    array(
	        'default'   => true, //Default setting/value to save
	        'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
	    )
	);
	$wp_customize->add_control(
	    'asu_research_post_timestamp', 
	    array(
	        'label'    => __( 'Timestamp', 'asu_research' ),
	        'section'  => 'asu_research_post_presets_options',
	        'settings' => 'asu_research_post_timestamp',
	        'type'     => 'radio',
	        'choices'  => array(
				true      => 'Show',
				false 	  => 'Hide'
	        ),
	    )
	);
	//Post Preset - Post Timestamp
	$wp_customize->add_setting('asu_research_post_featured_image',
	    array(
	        'default'   => true, //Default setting/value to save
	        'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
	    )
	);
	$wp_customize->add_control(
	    'asu_research_post_featured_image', 
	    array(
	        'label'    => __( 'Featured Image', 'asu_research' ),
	        'section'  => 'asu_research_post_presets_options',
	        'settings' => 'asu_research_post_featured_image',
	        'type'     => 'radio',
	        'choices'  => array(
				true      => 'Show',
				false 	  => 'Hide'
	        ),
	    )
	);



	//Footer Preset Section
	$wp_customize->add_section( 'asu_research_footer_presets_options',
	    array(
	        'title'      => __('Footer Presets','asu_research'),
	        'priority'   => 30,
	    )
	);

	//Footer Preset - Footer Style
	$wp_customize->add_setting('asu_research_footer_preset',
	    array(
	        'default'   => 'footer-default', //Default setting/value to save
	        'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
	    )
	);
	$wp_customize->add_control(
	    'asu_research_footer_presets', 
	    array(
	        'label'    => __( 'Footer Theme', 'asu_research' ),
	        'section'  => 'asu_research_footer_presets_options',
	        'settings' => 'asu_research_footer_preset',
	        'type'     => 'radio',
	        'choices'  => array(
				'footer-default'      => 'Dark',
				'footer-light'   	  => 'Light'
	        ),
	    )
	);

	//Footer Preset - Widget Grid
	$wp_customize->add_setting('asu_research_footer_widget_preset',
	    array(
	        'default'   => '', //Default setting/value to save
	        'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
	    )
	);
	$wp_customize->add_control(
	    'asu_research_footer_widget_presets', 
	    array(
	        'label'    => __( 'Footer Widget Grid', 'asu_research' ),
	        'section'  => 'asu_research_footer_presets_options',
	        'settings' => 'asu_research_footer_widget_preset',
	        'type'     => 'radio',
	        'choices'  => array(
				''             => '3-3-3-3',
				'widget-4-5-3' => '4-5-3'
	        ),
	    )
	);
	
	//Footer Preset - Social Media
	$wp_customize->add_setting('asu_research_footer_social_preset',
	    array(
	        'default'   => true, //Default setting/value to save
	        'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
	    )
	);
	$wp_customize->add_control('asu_research_footer_social_presets', 
	    array(
	        'label'    => __( 'Footer Social', 'asu_research' ),
	        'section'  => 'asu_research_footer_presets_options',
	        'settings' => 'asu_research_footer_social_preset',
	        'type'     => 'radio',
	        'choices'  => array(
				true  => 'Show',
				false => 'Hide'
	        ),
	    )
	);

	//Footer Preset - Last Updated
	$wp_customize->add_setting('asu_research_footer_updated_preset',
	    array(
	        'default'   =>  '', //Default setting/value to save
	        'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
	    )
	);

	$wp_customize->add_control('asu_research_footer_updated_presets', 
	    array(
	        'label'    => __( 'Footer Last Updated', 'asu_research' ),
	        'section'  => 'asu_research_footer_presets_options',
	        'settings' => 'asu_research_footer_updated_preset',
	        'type'     => 'radio',
	        'choices'  => array(
				true    => 'Show',
				''		=> 'Hide'
	        ),
	    )
	);

	//Logo Options
	$wp_customize->add_section('asu_research_logo_options',
        array(
            'title' => __('Logo', 'asu_research'),
            'priority' => 30,
            'description' => 'Upload a logo to replace the default site logo. Image size needs to be 1076x144',
        )
    );

    $wp_customize->add_setting('asu_research_logo',
        array(
            'sanitize_callback' => 'esc_url_raw',
        )
    );

    $wp_customize->add_control(
        new \WP_Customize_Image_Control(
            $wp_customize,
            'asu_research_logo',
            array(
                'label' => __('Logo', 'asu_research'),
                'section' => 'asu_research_header_presets_options',
                'settings' => 'asu_research_logo',
                'description' => '<p>Upload a logo to replace the default site logo. Image size needs to be 1076x144</p>',
            )
        )
    );
	$wp_customize->get_setting('blogname')->transport = 'postMessage';
}
add_action('customize_register', __NAMESPACE__ . '\\customize_register');

/**
 * Customizer JS
 */
function customize_preview_js() {
  wp_enqueue_script('sage/customizer', Assets\asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
}
add_action('customize_preview_init', __NAMESPACE__ . '\\customize_preview_js');
