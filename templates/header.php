<script>
//if page is in an iframe, script breaks the page out of iframe
if (top.frames.length!=0)
  top.location=self.document.location;
</script>
<header class="banner navbar navbar-default navbar-static-top <?php echo get_theme_mod('asu_research_header_preset', '' ); ?> <?php echo get_theme_mod('asu_research_menu_preset', '' ); ?>" role="banner">
  <div class="site-header container">
    <div class="navbar-header">
      <?php if (has_nav_menu('primary_navigation')) :?>
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <?php endif; ?>
      <button type="button" class="searchbar-toggle collapsed" data-toggle="collapse" data-target=".searchbar-collapse">
        <span class="sr-only"><?= __('Toggle search', 'sage'); ?></span>
        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
      </button>
      <?php if ( get_theme_mod( 'asu_research_logo' ) ) : ?>
        <a class="navbar-brand hidden-xs" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><img src="<?php echo esc_url( get_theme_mod( 'asu_research_logo' ) ); ?>" class="responsive" alt="<?php bloginfo('name'); ?>" width="538" height="70" /></a>
        <a class="navbar-brand-mobile visible-xs-block" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/asu-wpcarey-logo-mobile<?php echo (get_theme_mod('asu_research_header_preset') !== 'navbar-header-maroon'? '' : '-reversed'); ?>.png" class="responsive" alt="<?php bloginfo('name'); ?>" width="200" height="53" /></a>
      <?php else : ?>
        <a class="navbar-brand hidden-xs" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/asu-wpcarey-logo<?php echo (get_theme_mod('asu_research_header_preset') !== 'navbar-header-maroon'? '' : '-reversed'); ?>.png" class="responsive" alt="<?php bloginfo('name'); ?>" width="294" height="70" /></a>
        <a class="navbar-brand-mobile visible-xs-block" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/asu-wpcarey-logo-mobile<?php echo (get_theme_mod('asu_research_header_preset') !== 'navbar-header-maroon'? '' : '-reversed'); ?>.png" class="responsive" alt="<?php bloginfo('name'); ?>" width="200" height="53" /></a>
      <?php endif; ?>
      <h1 class="mobile-header <?php echo get_theme_mod('asu_research_header_name_preset'); ?>"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo('name'); ?></a></h1>
    </div>
    <nav id="universal" role="navigation">
      <ul id="menu-universal-menu" class="nav navbar-nav universal hidden-xs">
          <li class="menu-w-p-carey-home"><a href="https://wpcarey.asu.edu">W. P. Carey Home</a></li>
          <li class="menu-directory"><a href="https://wpcarey.asu.edu/people/directory">Directory</a></li>
          <li class="dropdown menu-research-centers"><a class="dropdown-toggle" aria-haspopup="true" aria-expanded="false"data-toggle="dropdown" data-target="#" href="#">Research Centers <span class="caret"></span></a>
              <ul class="dropdown-menu">
                  <li><a href="http://casee.asu.edu/" target="_blank">Center for the Advanced Study in Economic Efficiency (CASEE)</a></li>
                  <li><a href="https://wpcarey.asu.edu/research/competitiveness-prosperity-research/" target="_blank">Center for Competitiveness and Prosperity Research</a></li>
                  <li><a href="https://wpcarey.asu.edu/research/entrepreneurship" target="_blank">Center for Entrepreneurship</a></li>
                  <li><a href="https://research.wpcarey.asu.edu/ceesp/" target="_blank">Center for Environmental Economics and Sustainability Policy</a></li>
                  <li><a href="https://wpcarey.asu.edu/research/entrepreneurship" target="_blank">Center for Entrepreneurship</a></li>
                  <li><a href="https://research.wpcarey.asu.edu/real-estate" target="_blank">Center for Real Estate Theory and Practice</a></li>
                  <li><a href="https://wpcarey.asu.edu/research/services-leadership/" target="_blank">Center for Services Leadership</a></li>
                  <li><a href="http://www.capsresearch.org/" target="_blank">CAPS Research</a></li>
                  <li><a href="https://research.wpcarey.asu.edu/economic-liberty/" target="_blank">Center for the Study of Economic Liberty</a></li>
                  <li><a href="https://research.wpcarey.asu.edu/economic-outlook/" target="_blank">JPMorgan Chase Economic Outlook Center</a></li>
                  <li><a href="http://seidmaninstitute.com/" target="_blank">L. William Seidman Research Institute</a></li>
              </ul>
          </li>
      </ul>
    </nav>
    <div class="collapse searchbar-collapse">
      <?php get_template_part('templates/searchform'); ?>
    </div>
  </div>
  <div class="mainnav-wrapper">
    <div class="container">
      <nav class="collapse navbar-collapse <?php echo get_theme_mod('asu_research_menu_layout_preset', ''); ?>" role="navigation">
          <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new wp_bootstrap_navwalker(), 'menu_class' => 'nav navbar-nav']);
          endif;
          ?>
      </nav>
    </div>
  </div>
</header>
