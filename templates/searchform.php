<form role="search" method="get" class="search-form" action="<?= esc_url(home_url('/')); ?>">
	<label for="s" class="screen-reader-text"><?php _e('Search for:', 'sage'); ?></label>
	<div class="input-group">
		<input type="search" class="form-control search-field" placeholder="<?php _e('Search', 'sage'); ?> <?php bloginfo('name'); ?>" value="<?php get_search_query(); ?>" name="s" title="Search for:">
   		<span class="input-group-btn">
   			<button type="submit" class="search-submit btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"><span class="sr-only"><?php _e('Search', 'sage'); ?></span></span></button>
   		</span>
   	</div>
</form>