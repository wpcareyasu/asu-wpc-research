<?php
	$img_id = get_post_thumbnail_id($post->ID);
	$alt_text = get_post_meta($img_id , '_wp_attachment_image_alt', true);
	$alt_text = ($alt_text? $alt_text : '');	
?>
<div class="row">
	<div class="col-sm-10 col-sm-offset-1 col-md-offset-0 col-md-12">
		<article id="featured-post" <?php post_class('feed'); ?>>
			<div class="row no-gutters">
				<?php if (has_post_thumbnail()):?>
				<figure class="col-sm-12 col-md-6">
					<a href="<?php the_permalink(); ?>"><img src="<?php echo the_post_thumbnail_url( 'featured' ) ?>" alt="<?php echo $alt_text ?>" style="width:100%;"></a>
				</figure>	
				<section class="col-sm-12 col-md-6">
				<?php else:  ?>
				<section class="col-sm-12">
				<?php endif; ?>
					<div class="section-inner">
					  <header> 
					  	<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title();	 ?></a></h2>
					  	<?php //get_template_part('templates/entry-meta'); ?>
					  </header>
					  <div class="entry-summary">
					  	<?php the_excerpt(); ?>
					  	<a href="<?php the_permalink(); ?>" class="btn btn-primary pull-right">Read more</a>
					  </div>
					</div>
				</section>
			</div>
		</article>
	</div>
</div>