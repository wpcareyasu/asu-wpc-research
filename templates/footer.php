<footer class="content-info <?php echo get_theme_mod('asu_research_footer_preset', 'footer-default')?> <?php echo get_theme_mod('asu_research_footer_widget_preset')?>" role="contentinfo">
    <div class="container">
      <div class="row">
        <section class="widget widget_text">
          <div class="textwidget">
              <p><a href="https://wpcarey.asu.edu" target="_blank" class="branded-logo img-link"><img width="241" height="65" src="<?php echo get_template_directory_uri(); ?>/dist/images/asu-wpcarey-logo<?php echo (get_theme_mod('asu_research_footer_preset') === 'footer-light'? '2' : '-reversed2'); ?>.png" alt="W. P. Carey School of Business | Arizona State University" scale="0"></a></p>
              <p><a href="https://wpcarey.asu.edu/about" target="_blank">Contact Us</a></p>
              <?php if ( get_theme_mod('asu_research_footer_social_preset', true) ): ?>
              <div class="social-media">
                  <a href="http://facebook.com/wpcareyschool" target="_blank">
                      <i class="fa fa-2x fa-facebook"><span class="sr-only">Facebook</span></i>
                  </a>
                  <a href="http://twitter.com/wpcareyschool" target="_blank">
                      <i class="fa fa-2x fa-twitter"><span class="sr-only">Twitter</span></i>
                  </a>
                  <a href="http://www.linkedin.com/groups?gid=43848" target="_blank">
                      <i class="fa fa-2x  fa-linkedin"><span class="sr-only">LinkedIn</span></i>
                  </a>
                  <a href="https://youtube.com/wpcareyschool" target="_blank">
                      <i class="fa fa-2x fa-youtube"><span class="sr-only">Youtube</span></i>
                  </a>
                  <a href="https://instagram.com/wpcareyschool/" target="_blank">
                      <i class="fa fa-2x fa-instagram"><span class="sr-only">Instagram</span></i>
                  </a>
              </div>
              <?php endif; ?>
              <?php if (get_theme_mod('asu_research_footer_updated_preset', false)): ?>
               <p>Last Updated:<br>
              <?php
                  global $wpdb;
                
                  $query = "SELECT post_modified
                      FROM $wpdb->posts
                      ORDER BY post_date DESC
                    ";
                  //Using $wpdb for a cached result
                  $site_mod_date = $wpdb->get_var( $query );
                
                  $date_format = get_option( 'date_format' ) . ' - ' . get_option( 'time_format' );
                
                  echo mysql2date( $date_format, $site_mod_date );
              ?></p>
              <?php endif; ?>
          </div>
        </section>
        <?php dynamic_sidebar('sidebar-footer'); ?>
      </div>
  </div>
</footer>
<div class="asu-footer">
    <div class="little-foot">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="little-foot-nav">
                        <li><a href="http://www.asu.edu/copyright/">Copyright &amp; Trademark</a></li>
                        <li><a href="http://www.asu.edu/accessibility/">Accessibility</a></li>
                        <li><a href="http://www.asu.edu/privacy/">Privacy</a></li>
                        <li><a href="https://cfo.asu.edu/hr-applicant">jobs@asu</a></li>
                        <li><a href="http://www.asu.edu/emergency/">Emergency</a></li>
                        <li class="no-border"><a href="http://www.asu.edu/contactasu/">Contact ASU</a></li>
                    </ul>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>
</div>