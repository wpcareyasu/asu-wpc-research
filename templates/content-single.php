<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <header class="post-header">
    <?php if ( get_the_category() ){ ?>
      <a href="<?php $category = get_the_category(); echo get_category_link( $category[0]->cat_ID); ?>" class="entry-category">
        <?php echo $category[0]->name; ?>
      </a>
    <?php } ?>
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <?php get_template_part('templates/entry-meta'); ?>
      <?php if(get_theme_mod('asu_research_post_excerpt', true) && has_excerpt()) {?><div class="excerpt"><?php the_excerpt()?></div><?php } ?>
    </header>
    <?php if(has_post_thumbnail() && get_theme_mod('asu_research_post_featured_image', true)) :?>
      <figure class="featured">
        <?php the_post_thumbnail('featured-retina', array( 'class' => 'img-responsive' )); ?>
      </figure>
    <?php endif;?>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
    <?php if(get_theme_mod('asu_research_post_tags', true)){ ?>
    <div class="entry-tags">
      <?php the_tags( 'Tags: ', ', ', '' );?>
    </div>
    <?php } ?>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
  </article>
  <?php comments_template('/templates/comments.php'); ?>
<?php endwhile; ?>
