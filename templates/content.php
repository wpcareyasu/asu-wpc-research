<?php
	$img_id = get_post_thumbnail_id($post->ID);
	$alt_text = get_post_meta($img_id , '_wp_attachment_image_alt', true);
	$alt_text = ($alt_text? $alt_text : '');	
?>
<article <?php post_class('feed'); ?>>
	<div class="row">
		<?php if (has_post_thumbnail()) : ?>
		<figure class="col-xs-12 col-sm-6 col-md-4">
			<a href="<?php the_permalink(); ?>"><img src="<?php echo the_post_thumbnail_url( 'feed' ) ?>" alt="<?php echo $alt_text ?>" style="width:100%;"></a>
		</figure>	
		<section class="col-xs-12 col-sm-6 col-md-8">
		<?php else:  ?>
		<section class="col-xs-12">
		<?php endif; ?>
			<div class="section-inner">
		  		<header> 
		  			<h2 class="h3 entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		  			<?php //get_template_part('templates/entry-meta'); ?>
		  		</header>
		  		<div class="entry-summary">
		  			<?php the_excerpt(); ?>
		  		</div>
		  	</div>
		</section>
	</div>
</article>